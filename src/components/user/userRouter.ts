import express from 'express'
const userRouter = express.Router()
import { registerUserController, loginUserController } from './userController'


userRouter.post('/register', registerUserController)
userRouter.post('/login', loginUserController)


export default userRouter