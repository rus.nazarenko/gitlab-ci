// const db = require("../../db/models")
// const bcrypt = require('bcrypt')
// const jwt = require('jsonwebtoken')
// const { secret } = require('../../config/server')
import { User } from "./userTypes"
import db from '../../db/models'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import { secret } from '../../config/server'


export const registerUserService = async (data: User) => {
  try {
    const currenUser = await db.user.findOne({ where: { email: data.email } })
    if (currenUser) throw new Error("This email is already registered")
    data.password = await bcrypt.hash(data.password, 10)
    const result = await db.user.create(data)
    return result
  } catch (err) {
    throw new Error(err)
  }
}


export const loginUserService = async (data: User) => {
  try {
    const currenUser = await db.user.findOne({ where: { email: data.email } })
    if (!currenUser) throw new Error("Wrong email")
    const passwordTrue = await bcrypt.compare(data.password, currenUser.password)
    if (!passwordTrue) throw new Error("Wrong password")
    const token = jwt.sign(currenUser.dataValues.email, secret)
    return token
  } catch (err) {
    throw new Error(err)
  }
}