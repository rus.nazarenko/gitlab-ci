import { secret } from '../config/server'
import jwt from 'jsonwebtoken'
import { Request, Response, NextFunction } from 'express'

export const tokenVerification = (req: Request, res: Response, next: NextFunction) => {
  try {
    if (!req.headers.authorization && !req.cookies.token) throw new Error()
    const decodedToken = jwt.verify(req.headers.authorization || req.cookies.token.substr(7), secret)
    next()
  } catch (err) {
    res.status(403).send('Token is not valid')
  }
}